# Development
FROM ubuntu:18.04

ENV SCALA_VERSION 2.13.8
ENV SBT_VERSION 1.7.1

ENV LIBVDRTOOLS_VERSION 0.8.6~1711-bionic
ENV LIBVCX_VERSION 0.14.1-bionic~844

ENV DEBIAN_FRONTEND="noninteractive"
ENV TZ="Etc/UTC"

# Install required and useful packages
# [NOTE] this is all on one line (so one layer) to avoid apt caching issues
#        See best practices here:
#          https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
RUN apt-get update && \
    #Install base packages for later apt steps
    apt-get install -y \
      ca-certificates \
      curl \
      gnupg \
      software-properties-common && \
    # Add Evernym CA cert
    mkdir -p /usr/local/share/ca-certificates && \
    curl -k https://repo.corp.evernym.com/ca.crt | tee /usr/local/share/ca-certificates/Evernym_Root_CA.crt && \
    # Update CA certificates to trust Evernym's cert
    update-ca-certificates && \
    # Add mono repository
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF && \
    add-apt-repository "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" && \

    # Adding Evernym public key & repo
    curl https://repo.corp.evernym.com/repo.corp.evenym.com-sig.key | apt-key add - && \
    add-apt-repository "deb https://repo.corp.evernym.com/deb evernym-agency-dev-ubuntu main" && \
    # Adding Nodejs public repo
    curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get update && \
    apt-get install --no-install-recommends -y \
    # Require packages:
      openjdk-11-jdk \
      # ⬇️ is required for integration tests
      awscli \    
      # ⬇️ is required for protobuf
      libatomic1 \
      # ⬇️ is required for debain package building
      fakeroot \
      # ⬇️ is required for aws cli install (see below)
      unzip \
      # ⬇️ is required sbt tasks (version & deploy)
      git \
      # ⬇️ is required for sdk tests
      build-essential \
      python3-setuptools \
      python3-wheel \
      nodejs \
      python3 \
      python3-pip \
    # Useful packages
      dnsutils \
      lsof \
      mysql-client \
      python \
      time \
      netcat \
      vim \
    # Install versioned packages
    # Require packages:
      libvdrtools=${LIBVDRTOOLS_VERSION} \
      libvcx=${LIBVCX_VERSION} \
    # Useful packages
    #  indy-cli=${LIBINDY_VERSION}
    # Install .NET CLI dependencies and Mono
      libc6 \
      libgcc1 \
      libgssapi-krb5-2 \
      libicu60 \
      libssl1.1 \
      libstdc++6 \
      mono-devel \
      zlib1g && \
    # Clear off apt cached files
    rm -rf /var/lib/apt/lists/*

# virtualenv is used for sdk integration tests
RUN pip3 install virtualenv

ENV \
    # Enable detection of running in a container
    DOTNET_RUNNING_IN_CONTAINER=true \
    # Enable correct mode for dotnet watch (only mode supported in a container)
    DOTNET_USE_POLLING_FILE_WATCHER=true \
    # Skip extraction of XML docs - generally not useful within an image/container - helps performance
    NUGET_XMLDOC_MODE=skip \
    # PowerShell telemetry for docker image usage
    POWERSHELL_DISTRIBUTION_CHANNEL=PSDocker-DotnetCoreSDK-Ubuntu-18.04

# Install .NET Core SDK
RUN dotnet_sdk_version=3.1.404 \
    && curl -SL --output dotnet.tar.gz https://dotnetcli.azureedge.net/dotnet/Sdk/$dotnet_sdk_version/dotnet-sdk-$dotnet_sdk_version-linux-x64.tar.gz \
    && dotnet_sha512='94d8eca3b4e2e6c36135794330ab196c621aee8392c2545a19a991222e804027f300d8efd152e9e4893c4c610d6be8eef195e30e6f6675285755df1ea49d3605' \
    && echo "$dotnet_sha512 dotnet.tar.gz" | sha512sum -c - \
    && mkdir -p /usr/share/dotnet \
    && tar -ozxf dotnet.tar.gz -C /usr/share/dotnet \
    && rm dotnet.tar.gz \
    && ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet \
    # Trigger first run experience by running arbitrary cmd
    && dotnet help

# Install dotnet-script for integration tests
RUN dotnet tool install -g dotnet-script

# Install datadog uploader for Unit Test results. See https://app.datadoghq.com/ci/test-runs
RUN npm install -g @datadog/datadog-ci

# Install SBT from GitHub
RUN curl -sL https://github.com/sbt/sbt/releases/download/v${SBT_VERSION}/sbt-${SBT_VERSION}.tgz | tar -xz && \
  mv sbt /opt && \
  ln -s /opt/sbt/bin/sbt /usr/local/bin && \
  mkdir -p /etc/sbt

# Build dummy sbt project to cache sbt downloads
WORKDIR /root
RUN \
  sbt new scala/scala-seed.g8 --name=dummy && \
  cd dummy && \
  sed -i "/ThisBuild \/ scalaVersion/c\ThisBuild / scalaVersion     := \"${SCALA_VERSION}\"" build.sbt && \
  echo "sbt.version=${SBT_VERSION}" > project/build.properties && \
  sbt compile && \
  rm -rf /root/dummy

# Options for SBT
# 1)   move ivy cache for gitlab CI/CD
# 2)   give more memory for sbt
# 3)   This will force sbt to use java sdk getLastModifiedTime instead of os native method. This helps
#      with incremental compile. We don't need millisecond precision because files in this container are static.
#      See: https://stackoverflow.com/questions/54068316/how-to-re-use-compiled-sources-in-different-machines
# 4-6) Compensate for a bug in sbt that can't handle a 'java.io.tmpdir' that is long. This causes the socket to
#      to be created and the boot server to not start. The parameters are intended to stop the boot server to be started
#      (but that don't seem to work) and allow sbt to start even if the socket is not created.
RUN \
  echo '-ivy ${CI_PROJECT_DIR:-$HOME}/.ivy2' >> /etc/sbt/sbtopts && \
  echo '-mem 2048'                           >> /etc/sbt/sbtopts && \
  echo '-Dsbt.io.jdktimestamps=true'         >> /etc/sbt/sbtopts && \
  echo '-Dsbt.io.virtual=false'              >> /etc/sbt/sbtopts && \
  echo '-Dsbt.server.autostart=false'        >> /etc/sbt/sbtopts && \
  echo '-Dsbt.server.forcestart=true'        >> /etc/sbt/sbtopts


# Install ngrok
RUN curl -O https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip && \
  unzip ngrok-stable-linux-amd64.zip && \
  cp ngrok /usr/local/bin/. && \
  rm -rf ngrok*
